const express = require('express');
const path = require('path');
const bodyParser = require("body-parser");

const server = express();

server.use(bodyParser.urlencoded({ extended: true }));

server.use(express.static(path.join(__dirname, 'public')));

server.set('view engine', 'ejs');

const clientes = require('./Clientes')
// server.use('/clientes', clientes)
server.use(clientes)

server.get('/', (req,res) => {
    // res.send('server OK');
    res.redirect('/clientes')
});

server.listen(8080,()=>{ console.log('Rodando server CRUD...'); })