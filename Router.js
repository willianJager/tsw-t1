const express = require('express');
const router = express.Router();
const dados = require('./Data');

router.get('/clientes', async (req, res) => {
    // res.send("entrou em /clientes")
    let cli = new dados.Router();
    res.render('home', { cliente: cli, lista: await dados.GetListaClientes() });
})

router.get('/clientes/:id', async (req, res) => {
    let id = req.params.id;
    let cli = dados.CarregaClientePorCodigo(id);
    res.render('home', { cliente: cli, lista: await dados.GetListaClientes() });
})

router.post('/clientes/del/:id', async (req, res) => {
    let id = req.params.id;
    await dados.DeletaClientePorCodigo(id);
    res.redirect('/clientes');
})

router.post('/clientes', async (req, res) => {
    var erros = [];
    var erroMsg = '';
    let cli = new dados.Router();

    cli.codigo = req.body.codigo;
    cli.nome = req.body.nome;

    if (cli.codigo != '' && cli.nome != '') {
        await dados.AddClientes(cli);
        cli = new dados.Router();
    } else {
        if (cli.codigo == '') {
            erros.push('Código')
        }
        if (cli.nome == '') {
            erros.push('Nome')
        }
        if (erros.length > 0) {
            erroMsg = 'Informe os campos (' + erros.join(',') + ')';
        }
    }
    res.render('home', { cliente: cli, lista: await dados.GetListaClientes(), erroMsg: erroMsg });
});

module.exports = router;