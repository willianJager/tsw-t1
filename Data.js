const fs = require('fs');
const util = require('util');
const NOME_ARQUIVO = 'lista.txt';

function Router() {
    this.codigo = '',
        this.nome = ''
}
// var a = {codigo : 0, nome : ''};

var lista = [];

async function GetListaClientes() {
    await CarregarArquivo();
    return lista;
}

async function AddClientes(cliente) {
    let existe = false;
    for (let i = 0; i < lista.length; i++) {
        if (lista[i].codigo == cliente.codigo) {
            lista[i] = cliente;
            existe = true;
            break;
        }
    }
    if (!existe)
        lista.push(cliente);
    await SalvarArquivo();
}

async function SalvarArquivo() {
    let data = JSON.stringify(lista);
    // fs.writeFileSync(NOME_ARQUIVO,data);
    await util.promisify(fs.writeFile)(NOME_ARQUIVO, data)
}

async function CarregarArquivo() {
    // let data = fs.readFileSync(NOME_ARQUIVO);
    // lista = JSON.parse(data.toString());
    // console.log(1);
    // fs.readFile(NOME_ARQUIVO,(error,data)=>{
    //     lista = JSON.parse(data.toString());
    //     console.log('entrou');
    // })
    // console.log(util.promisify(fs.readFile)(NOME_ARQUIVO));
    // util.promisify(fs.readFile)(NOME_ARQUIVO).then((data)=>{console.log(data.toString());}).catch()
    try {
        let stat = await util.promisify(fs.stat)(NOME_ARQUIVO);
        if (stat.isFile) {
            let data = await util.promisify(fs.readFile)(NOME_ARQUIVO);
            // console.log(data.toString());
            lista = JSON.parse(data.toString());
        }
    } catch (error) { }
    // console.log(2);
}

function CarregaClientePorCodigo(id) {
    for (let i = 0; i < lista.length; i++) {
        if (lista[i].codigo == id) {
            return lista[i];
        }
    }
    return new Router();
}

async function DeletaClientePorCodigo(id) {
    for (let i = 0; i < lista.length; i++) {
        if (lista[i].codigo == id) {
            lista.splice(i, 1);
            await SalvarArquivo();
            break;
        }
    }
}

module.exports.Router = Router;
module.exports.GetListaClientes = GetListaClientes;
module.exports.AddClientes = AddClientes;
module.exports.CarregaClientePorCodigo = CarregaClientePorCodigo;
module.exports.DeletaClientePorCodigo = DeletaClientePorCodigo;